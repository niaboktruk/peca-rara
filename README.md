# Peça Rara

Portal de serviços para as lojas Peça Rara. Gerado pelo [Angular CLI] versão 8.0.3.

## Servidor de desenvolvimento

Instale o [NODE] de acordo com as instruções (https://nodejs.org/en/) que já vem com a ferramenta [NPM] para instalar pacotes.

Instale o [Angular/Cli] com o seguinte comando `npm install -g @angular/cli`.

Instale o [Git] (https://git-scm.com/downloads).

Vá até a pasta de projetos do seu computador e digite o comando `git clone https://gitlab.com/niaboktruk/tranquilos-city.git`.

Entre na pasta do projeto e digite `npm install` que ele vai instalar todos os componentes que o projeto utiliza.

Digite `ng serve` para criar um servidor de desenvolvimento. Navegue em `http://localhost:4200/`. A aplicação irá atualizar automaticamente a cada alteração nas páginas.
