﻿export class User {
    id: number;
    apelido: string;
    password: string;
    nome: string;
    sobrenome: string;
    loja_id: number;
    token: string;
    profile_sign_url: string;
    responsavel_loja: {
      nome_completo: string;
    };
}
