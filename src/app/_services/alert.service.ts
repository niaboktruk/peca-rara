﻿import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Router, NavigationStart } from '@angular/router'
import { environment } from '@environments/environment'
import { Observable, Subject } from 'rxjs'

@Injectable({ providedIn: 'root' })
export class AlertService {
  private subject = new Subject<any>();
  private keepAfterRouteChange = false;

  constructor(private router: Router, private http: HttpClient) {
    // clear alert messages on route change unless 'keepAfterRouteChange' flag is true
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (this.keepAfterRouteChange) {
          // only keep for a single route change
          this.keepAfterRouteChange = false
        } else {
          // clear alert message
          this.clear()
        }
      }
    })
  }

  getAlert(): Observable<any> {
    return this.subject.asObservable()
  }

  success(message: string, keepAfterRouteChange = false) {
    this.keepAfterRouteChange = keepAfterRouteChange
    this.subject.next({ type: 'success', text: message })
  }

  error(error: any, keepAfterRouteChange = false) {
    if (error.status !== 404) this.http.post(`${environment.apiUrl}/v2/ged/errors`, { msg: error} ).subscribe()

    let message = 'Houve um erro no sistema. Por favor tente mais tarde.'
    console.log('error', error.error)

    if (typeof error.error === 'string') console.log('error is string', error.error)
    else message = this.getProperty(error.error, 'message')

    console.log('message', message)

    //if (error.status !== 500 && (error?.message||error.error?.message||error.error?.errors.login||error.error?.errors.value))
    //   message = error?.message||error.error?.message||error.error?.errors.login||error.error?.errors.value

    this.keepAfterRouteChange = keepAfterRouteChange
    this.subject.next({ type: 'error', text: message })
  }

  getProperty(obj, prop) {
    for (let key in obj) {
      if (key === prop) {
        return obj[key];
      }
      if (obj[key] !== null && typeof obj[key] === 'object') {
        const result = this.getProperty(obj[key], prop);
        if (result !== undefined) {
          return result;
        }
      }
    }
  }

  clear() {
    // clear by calling subject.next() without parameters
    this.subject.next()
  }
}
