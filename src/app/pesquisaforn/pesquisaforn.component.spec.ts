import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesquisafornComponent } from './pesquisaforn.component';

describe('PesquisafornComponent', () => {
  let component: PesquisafornComponent;
  let fixture: ComponentFixture<PesquisafornComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesquisafornComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesquisafornComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
