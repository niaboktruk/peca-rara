import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompraassinaturaComponent } from './compraassinatura.component';

describe('CompraassinaturaComponent', () => {
  let component: CompraassinaturaComponent;
  let fixture: ComponentFixture<CompraassinaturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompraassinaturaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompraassinaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
