import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtratodetalheComponent } from './extratodetalhe.component';

describe('ExtratodetalheComponent', () => {
  let component: ExtratodetalheComponent;
  let fixture: ComponentFixture<ExtratodetalheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtratodetalheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtratodetalheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
