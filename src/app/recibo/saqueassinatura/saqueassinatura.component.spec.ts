import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaqueassinaturaComponent } from './saqueassinatura.component';

describe('SaqueassinaturaComponent', () => {
  let component: SaqueassinaturaComponent;
  let fixture: ComponentFixture<SaqueassinaturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaqueassinaturaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaqueassinaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
